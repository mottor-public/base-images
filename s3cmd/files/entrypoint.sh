#!/bin/bash -e

ME=$(basename $0)
S3CMD_PATH=/opt/s3cmd/s3cmd
S3CMD_CONFIG=/root/.s3cfg
ENV_LIST=("bucket_location" "gpg_passphrase" "host_base" "host_bucket" "access_key" "secret_key" "verbosity")

log() {
    echo -e "$ME: $@"
}

log "Start"

if [ -z "${S3CMD_ACCESS_KEY}" ]; then
    log "ERROR: The environment variable S3CMD_ACCESS_KEY is not set."
    exit 1
fi

if [ -z "${S3CMD_SECRET_KEY}" ]; then
    log "ERROR: The environment variable S3CMD_SECRET_KEY is not set."
    exit 1
fi

if [ -z "$S3CMD_HOST_BASE" ]; then
  export S3CMD_HOST_BASE="s3.amazonaws.com"
  log "env S3CMD_HOST_BASE was set to default value '$S3CMD_HOST_BASE'"
fi

log "Patching .s3cfg:"
for name in "${ENV_LIST[@]}"; do
  env_name=$(echo "S3CMD_${name}" | tr '[a-z]' '[A-Z]')
  if [ "${!env_name}" != "" ]; then
    log " - applied env ${env_name}"
    sed -i "s/^${name} = .*/${name} = ${!env_name}/" "$S3CMD_CONFIG"
  fi
done

# Check whether to run a pre-defined command
#if [ -n "${cmd}" ]; then
#  #
#  # sync-s3-to-local - copy from s3 to local
#  #
#  if [ "${cmd}" = "sync-s3-to-local" ]; then
#      echo ${SRC_S3}
#      ${S3CMD_PATH} sync $* ${SRC_S3} /opt/dest/
#  fi
#
#  #
#  # sync-local-to-s3 - copy from local to s3
#  #
#  if [ "${cmd}" = "sync-local-to-s3" ]; then
#      ${S3CMD_PATH} sync $* /opt/src/ ${DEST_S3}
#  fi
#else
#  ${S3CMD_PATH} $*
#fi

log ""
log "Running s3cmd:"
${S3CMD_PATH} $*
