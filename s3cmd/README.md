# s3cmd

based on https://github.com/hochzehn/s3cmd-docker

~

всё, что есть в конфиге .s3cmd может быть заменено через ENV переменные.

имя переменной = `S3CMD_` + `название настройки из .s3cmd`

например, в файле есть `access_key = ....` - эту настройку можно задать через env `S3CMD_ACCESS_KEY`.
