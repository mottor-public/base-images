#!/bin/sh

if [ "$MEMCACHED_VERBOSE" = "" ]; then
  MEMCACHED_VERBOSE="-vv"
fi

/usr/bin/memcached \
  --user=memcached \
  --listen=0.0.0.0 \
  --port=11211 \
  --memory-limit=${MEMCACHED_MEMUSAGE:-64} \
  --conn-limit=${MEMCACHED_MAXCONN:-1024} \
  --threads=${MEMCACHED_THREADS:-4} \
  --max-reqs-per-event=${MEMCACHED_REQUESTS_PER_EVENT:-20} \
  "$MEMCACHED_VERBOSE"
