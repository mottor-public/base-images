#!/bin/bash

HOST="${1:-127.0.0.1}"
PORT="${2:-9000}"
PING_PATH="${3:-/ping}"
EXPECTED_RESPONSE="${4:-pong}"

env -i REDIRECT_STATUS=true SCRIPT_NAME="$PING_PATH" SCRIPT_FILENAME="$PING_PATH" REQUEST_METHOD=GET \
  cgi-fcgi -bind -connect $HOST:$PORT | grep -q "$EXPECTED_RESPONSE" || exit 1
