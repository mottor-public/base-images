#!/bin/bash
export ME=$(basename $0)

# Vars to set:
# PHP_READINESS_MAX_LOAD_PERCENT
# PHP_READINESS_CUSTOM_SCRIPT
# PHP_READINESS_DEBUG

HOST="${1:-127.0.0.1}"
PORT="${2:-9000}"
PHP_POOL_CONFIG_FILE="${3:-$PHP_FPM_CONFIGS_DIR/www.conf}"

DEFAULT_MAX_CHILDREN=50
MAX_LOAD_PERCENT=${PHP_READINESS_MAX_LOAD_PERCENT:-95}

# --------------------------------------

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
#BLUE='\033[0;34m'
#PURPLE='\033[0;35m'
#CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;37m'
NC='\033[0m' # No Color

log_debug() {
    if [ "$PHP_READINESS_DEBUG" == "1" ]; then
        echo -e "${LIGHT_GRAY}${ME}:${NC} ${LIGHT_GRAY}$@${NC}"
    fi
}

log_debug_green() {
    if [ "$PHP_READINESS_DEBUG" == "1" ]; then
        echo -e "${LIGHT_GRAY}${ME}:${NC} ${GREEN}$@${NC}"
    fi
}

log_warning() {
    echo -e "${LIGHT_GRAY}${ME}:${NC} ⚠️  ${YELLOW}WARNING! $@${NC}"
}

log_error() {
    echo -e "${LIGHT_GRAY}${ME}:${NC} ❌ ${RED}ERROR! $@${NC}"
}

exit_with_warning() {
    log_warning "${1}"
    log_warning "END -> FAIL"
    exit 1
}

exit_with_error() {
    log_error "${1}"
    log_warning "END -> FAIL"
    exit 1
}

export -f log_debug
export -f log_debug_green
export -f log_warning
export -f log_error
export -f exit_with_warning
export -f exit_with_error

# Функция для проверки, может ли текущий пользователь выполнить файл
can_user_execute() {
    file="$1"
    user_id=$(id -u)
    group_id=$(id -g)
    file_info=$(stat -c "%a %u %g" "$file")

    permissions=$(echo "$file_info" | awk '{print $1}')
    owner_id=$(echo "$file_info" | awk '{print $2}')
    group_id_file=$(echo "$file_info" | awk '{print $3}')

    owner_permissions=$(( (permissions / 100) % 10 ))
    group_permissions=$(( (permissions / 10) % 10 ))
    other_permissions=$(( permissions % 10 ))

    # Проверка прав на выполнение для владельца
    if [ "$user_id" -eq "$owner_id" ] && [ $((owner_permissions & 1)) -eq 1 ]; then
        return 0
    fi

    # Проверка прав на выполнение для группы
    if [ "$group_id" -eq "$group_id_file" ] && [ $((group_permissions & 1)) -eq 1 ]; then
        return 0
    fi

    # Проверка прав на выполнение для других пользователей
    if [ $((other_permissions & 1)) -eq 1 ]; then
        return 0
    fi

    return 1
}

# --------------------------------------
log_debug_green "START"

# Получаем количество активных процессов
export ACTIVE_PROCESSES=$(env -i REDIRECT_STATUS=true SCRIPT_NAME=/status SCRIPT_FILENAME=/status REQUEST_METHOD=GET cgi-fcgi -bind -connect $HOST:$PORT | grep -E "^active processes:" | cut -d ":" -f2 | tr -d ' ')
ACTIVE_PROCESSES_IS_INT=$(echo "$ACTIVE_PROCESSES" | grep -qE '^[0-9]+$' && echo 'Y' || echo 'N')
if [ "$ACTIVE_PROCESSES_IS_INT" != "Y" ]; then
    exit_with_error "ACTIVE_PROCESSES expected to be an integer. Actual: '$ACTIVE_PROCESSES'."
fi

if [ ! -z "$PHP_READINESS_MAX_CHILDREN" ]; then
    export MAX_CHILDREN="$PHP_READINESS_MAX_CHILDREN"
else
    # Получаем max_children из конфигурации (путь к файлу нужно указать корректно)
    export MAX_CHILDREN=$(grep -E "^pm.max_children" "$PHP_POOL_CONFIG_FILE" | cut -d '=' -f2 | tr -d ' ')
fi

if [ "$MAX_CHILDREN" = "" ]; then
    export MAX_CHILDREN=$DEFAULT_MAX_CHILDREN
fi

# Рассчитываем % от max_children
export THRESHOLD=$((MAX_CHILDREN * MAX_LOAD_PERCENT / 100))

log_debug "active_processes = $ACTIVE_PROCESSES, max_processes = $MAX_CHILDREN, max_load = ${MAX_LOAD_PERCENT}% (${THRESHOLD} processes)"

OK_TEXT="✅ OK"
FAIL_TEXT="❌ FAIL"
PROC_CHECK_TEXT="processes check ($ACTIVE_PROCESSES < $THRESHOLD)"
CUSTOM_SCRIPT_CHECK_TEXT="custom script check"

# Проверяем, не превышено ли пороговое значение
if [ "$ACTIVE_PROCESSES" -ge "$THRESHOLD" ]; then
    exit_with_warning "$PROC_CHECK_TEXT --> $FAIL_TEXT"
else
    log_debug "$PROC_CHECK_TEXT --> $OK_TEXT"
    if [ ! -z "$PHP_READINESS_CUSTOM_SCRIPT" ]; then
        if [ ! -e "$PHP_READINESS_CUSTOM_SCRIPT" ]; then
            exit_with_error "Variable PHP_READINESS_CUSTOM_SCRIPT is set to $PHP_READINESS_CUSTOM_SCRIPT, but this file does not exists."
        fi

        if can_user_execute "$PHP_READINESS_CUSTOM_SCRIPT"; then
            set +e # позволяем скрипту продолжать выполнение даже при ненулевых кодах выхода команд.
            "$PHP_READINESS_CUSTOM_SCRIPT"
            actual_exit_code=$? # эта команда должна идти СРАЗУ ПОСЛЕ выполнения $PHP_READINESS_CUSTOM_SCRIPT
            set -e # Восстановление предыдущего состояния
            expected_exit_code=0
            if [ $actual_exit_code -eq $expected_exit_code ]; then
                log_debug "$CUSTOM_SCRIPT_CHECK_TEXT --> $OK_TEXT"
            else
                exit_with_warning "$CUSTOM_SCRIPT_CHECK_TEXT --> $FAIL_TEXT. Script $PHP_READINESS_CUSTOM_SCRIPT returned exit code $actual_exit_code, expected - $expected_exit_code."
            fi
        else
            exit_with_warning "$CUSTOM_SCRIPT_CHECK_TEXT --> $FAIL_TEXT. Script $PHP_READINESS_CUSTOM_SCRIPT is not executable for user '$(whoami)' with uid=$(id -u), gid=$(id -g))."
        fi
    fi
fi

log_debug_green "END -> OK"
exit 0
