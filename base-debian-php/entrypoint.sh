#!/bin/bash

echo "---------- PHP entrypoint script ----------"

# ;max_input_nesting_level = 64
# ;user_agent="PHP"
# ;date.timezone =
# pdo_mysql.cache_size = 2000
# ;opcache.enable=1

php_ini_config_file="$PHP_INI_DIR/php.ini"
php_ini_config_settings=(
    "upload_max_filesize"
    "max_execution_time"
    "max_input_time"
    "max_input_nesting_level"
    "max_input_vars"
    "memory_limit"
    "post_max_size"
    "user_agent"
    "date.timezone"
    "pdo_mysql.cache_size"
    "opcache.enable"
)

php_fpm_config_file="$PHP_CONFIGS_DIR/php-fpm.conf"
# log_level = alert, error, warning, notice, debug
php_fpm_config_settings=(
    "log_level"
    "emergency_restart_threshold"
    "emergency_restart_interval"
    "process_control_timeout"
)

php_fpm_pool_config_file="$PHP_FPM_CONFIGS_DIR/www.conf"
php_fpm_pool_config_settings=(
    "pm.max_children"
    "pm.start_servers"
    "pm.min_spare_servers"
    "pm.max_spare_servers"
    "pm.process_idle_timeout"
    "pm.max_requests"
    "request_terminate_timeout"
)

#---------------------------------------------

generate_env_var_name() {
    local prefix="$1"
    local key="$2"
    echo "${prefix}_${key^^}" | sed 's/\./_/g'
}

echo "Updating php.ini Config:"
for key in "${php_ini_config_settings[@]}"; do
    env_var=$(generate_env_var_name "PHP_INI" "$key")
    if [ -n "${!env_var}" ]; then
        echo "> $env_var --> ${!env_var}"
        regex="$key\s*=.*!$key = ${!env_var}"
        sed -i "s!^;\s*$regex!" "$php_ini_config_file"
        sed -i "s!^$regex!" "$php_ini_config_file"
    else
        echo "> $env_var not set"
    fi
    grep -E --color "$key" "$php_ini_config_file"
done

echo "--------------------"

echo "Updating FPM php-fpm.conf Config:"
for key in "${php_fpm_config_settings[@]}"; do
    env_var=$(generate_env_var_name "PHP_FPM" "$key")
    if [ -n "${!env_var}" ]; then
        echo "> $env_var --> ${!env_var}"
        regex="$key\s*=.*/$key = ${!env_var}"
        sed -i "s/^;\s*$regex/" "$php_fpm_config_file"
        sed -i "s/^$regex/" "$php_fpm_config_file"
    else
        echo "> $env_var not set"
    fi
    grep -E --color "$key" "$php_fpm_config_file"
done

echo "--------------------"

echo "Updating FPM www.conf Config:"
for key in "${php_fpm_pool_config_settings[@]}"; do
    env_var=$(generate_env_var_name "PHP_FPM_POOL" "$key")
    if [ -n "${!env_var}" ]; then
        echo "> $env_var --> ${!env_var}"
        regex="$key\s*=.*/$key = ${!env_var}"
        sed -i "s/^;\s*$regex/" "$php_fpm_pool_config_file"
        sed -i "s/^$regex/" "$php_fpm_pool_config_file"
    else
        echo "> $env_var not set"
    fi
    grep -E --color "^$key" "$php_fpm_pool_config_file"
done

if [ -n "$PHP_FPM_POOL_DISABLE_FUNCTIONS" ]; then
  echo "> PHP_FPM_POOL_DISABLE_FUNCTIONS --> $PHP_FPM_POOL_DISABLE_FUNCTIONS"
  key="php_admin_value[disable_functions]"
  sed -i "s/^$key\s*=.*/$key = $PHP_FPM_POOL_DISABLE_FUNCTIONS/" "$php_fpm_pool_config_file"
else
  echo "> PHP_FPM_POOL_DISABLE_FUNCTIONS not set"
fi

echo "---------- PHP entrypoint script END ----------"

# Запуск CMD
exec "$@"
