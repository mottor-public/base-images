#!/bin/sh

ME=$(basename $0)
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;37m'
NC='\033[0m' # No Color
CHECK_MARK="\xE2\x9C\x85"
CROSS="\xE2\x9D\x8C"

log() {
  printf "%b" "${LIGHT_GRAY}${ME}:${NC} ${CYAN}"
  printf "%b" "$@"
  printf "%b" "${NC}\n"
}

logSuccess() {
  printf "%b" "${LIGHT_GRAY}${ME}:${NC} ${CHECK_MARK} ${GREEN}"
  printf "%b" "$@"
  printf "%b" "${NC}\n"
}

logError() {
  printf "%b" "${LIGHT_GRAY}${ME}:${NC} ${CROSS} ${RED}"
  printf "%b" "$@"
  printf "%b" "${NC}\n"
}

