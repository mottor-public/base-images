#!/bin/sh

CUR_DIR=$(cd "$(dirname "$0")" && pwd)

. $CUR_DIR/colors_log.sh

composer --version >/dev/null 2>&1 \
&& (logSuccess "composer is installed") \
|| (logError "ERROR: composer is not installed"; exit 1)
