#!/bin/sh

CUR_DIR=$(cd "$(dirname "$0")" && pwd)

. $CUR_DIR/colors_log.sh

MODULE_NAME="${1}"

php -m | grep -q "$MODULE_NAME" \
&& (logSuccess "module '$MODULE_NAME' is installed") \
|| (logError "ERROR: module '$MODULE_NAME' is not installed"; exit 1)
