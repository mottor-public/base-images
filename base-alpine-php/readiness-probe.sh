#!/bin/sh

HOST="${1:-127.0.0.1}"
PORT="${2:-9000}"

PHP_POOL_CONFIG_FILE="/usr/local/etc/php-fpm.d/zz-docker.conf"
DEFAULT_MAX_CHILDREN=5
MAX_LOAD_PERCENT=${PHP_READINESS_MAX_LOAD_PERCENT:-95}

# Получаем количество активных процессов
ACTIVE_PROCESSES=$(REDIRECT_STATUS=true SCRIPT_NAME=/status SCRIPT_FILENAME=/status REQUEST_METHOD=GET cgi-fcgi -bind -connect $HOST:$PORT | grep -E "^active processes:" | cut -d ":" -f2 | tr -d ' ')

if [ ! -z "$PHP_READINESS_MAX_CHILDREN" ]; then
  MAX_CHILDREN="$PHP_READINESS_MAX_CHILDREN"
else
  # Получаем max_children из конфигурации (путь к файлу нужно указать корректно)
  MAX_CHILDREN=$(grep -E "^pm.max_children" $PHP_POOL_CONFIG_FILE | cut -d '=' -f2 | tr -d ' ')
fi

if [ "$MAX_CHILDREN" = "" ]; then
  MAX_CHILDREN=$DEFAULT_MAX_CHILDREN
fi

# Рассчитываем 95% от max_children
THRESHOLD=$((MAX_CHILDREN * MAX_LOAD_PERCENT / 100))

# Проверяем, не превышено ли пороговое значение
if [ "$ACTIVE_PROCESSES" -ge "$THRESHOLD" ]; then
  echo "Exceeded the number of active php-fpm processes (Current: $ACTIVE_PROCESSES, Max: $MAX_CHILDREN)"
  exit 1
else
  echo "OK"
  exit 0
fi
