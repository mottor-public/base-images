#!/bin/sh

HOST="${1:-127.0.0.1}"
PORT="${2:-9000}"

REDIRECT_STATUS=true SCRIPT_NAME=/ping SCRIPT_FILENAME=/ping REQUEST_METHOD=GET \
  cgi-fcgi -bind -connect $HOST:$PORT | grep -q 'pong' || exit 1
