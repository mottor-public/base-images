#!/bin/sh

ME=$(basename $0)
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;37m'
NC='\033[0m' # No Color

log() {
  printf "%b" "${LIGHT_GRAY}${ME}:${NC} ${CYAN}"
  printf "%b" "$@"
  printf "%b" "${NC}\n"
}

#----------------------------

log " "
log "Cleaning image..."

if hash apt-get 2>/dev/null
then
  log "> apt-get clean"
  apt-get -y -qq --purge autoremove
  apt-get -y -qq clean
fi

if hash yum 2>/dev/null
then
  log "> yum clean"
  yum clean all
fi

if hash apk 2>/dev/null
then
  log "> apk clean"
  # https://gist.github.com/sgreben/dfeaaf20eb635d31e1151cb14ea79048#apk-del
  apk del --quiet --purge > /dev/null 2>&1
  apk cache --quiet clean > /dev/null 2>&1
fi

if hash pip 2>/dev/null
then
  log "> pip clean"
  pip cache purge
fi

log "> folders clean"
rm -Rf \
  /tmp/* \
  /var/cache/apk/* \
  /var/tmp/* \
  /var/lib/apt/lists/* \
  /var/log/alternatives.log \
  /var/log/apt/ \
  /var/log/bootstrap.log \
  /var/log/btmp \
  /var/log/dpkg.log \
  /var/log/faillog \
  /var/log/fsck/ \
  /var/log/lastlog \
  /var/log/wtmp \
  ~/.cache/pip

log "> /var/cache/ clean"
find /var/cache/ ! -type d -exec rm '{}' \;

log "Image cleaned!"
log " "
